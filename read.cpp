#include "read.hpp"

#include <optional>

using std::optional;

namespace scheme2llvm {

// predicate: datum is a non-starting/non-ending datum
optional<Datum> Reader::Stack::combineStackFront(Datum &&datum) {
  if (datum.kind == DK_Error) {
    return DK_Error;
  }
  while (true) {
    if (stack.empty()) {
      return std::move(datum);
    }
    switch (frontKind()) {
    case LK_Period: // .
      llvm::errs() << "LK_Period\n";
      if (empty()) {
        llvm::errs() << "return DK_Error 1 \n";
        return DK_Error;
      }
      if (LexemeKind k = frontKind();
          (k != LK_LeftParentheses && k != LK_LeftBracket) ||
          stack.front().last_period != StackNode::NoPeriod) {
          llvm::errs() << "frontKind(): " << k << '\n';
          llvm::errs() << "return DK_Error 2 \n";
        return DK_Error;
      }
      stack.front().last_period = StackNode::LastPeriod;
      break;
    case LK_LeftParentheses: // (
    case LK_LeftBracket:     // [
      if (datum.getKind() == DK_Period) {
        switch (stack.front().last_period) {
        case StackNode::NoPeriod:
          stack.front().last_period = StackNode::LastPeriod;
          return {};
        case StackNode::LastPeriod:
        case StackNode::MeetPeriod:
          return DK_Error;
        default:
          assert(false);
        }
      } else {
        switch (stack.front().last_period) {
        case Reader::StackNode::LastPeriod:
          stack.front().last_period = Reader::StackNode::MeetPeriod;
          [[fallthrough]];
        case Reader::StackNode::NoPeriod:
          frontAdd(std::move(datum));
          return {};
        case Reader::StackNode::MeetPeriod:
          return DK_Error;
        default:
          assert(false);
        }
      }
    case LK_PoundParentheses: // #(
      if (datum.getKind() == DK_Period) {
        return DK_Error;
      }
      frontAdd(std::move(datum));
      return {};
      break;
    case LK_Pound_vu8_Parentheses: // #vu8,#u8(
      if (datum.getKind() != DK_Number) {
        return DK_Error;
        ComplexNumber &number = datum.get<ComplexNumber>();
        if (number.cnk != CNK_Real || number.real_part.rnk != RNK_Integer) {
          return DK_Error;
        }
        frontAdd(std::move(datum));
        return {};
      }
      break;
    case LK_Apostrophe:      // '
    case LK_BackQuote:       // `
    case LK_Comma:           // ,
    case LK_CommaAt:         // ,@
    case LK_PoundApostrophe: // #'
    case LK_PoundBackQuote:  // #`
    case LK_PoundComma:      // #,
    case LK_PoundCommaAt:    // #,@
    {
      LexemeKind kind = frontKind();
      pop();
      Abbreviation tmp(lexemeToAreviationKind(kind), std::move(datum));
      datum = Datum(tmp);
      continue;
    }
    case LK_PoundSemicolon: // #;
      stack.pop_front();
      break;
    default:
      assert(false);
      return DK_Error;
    }
  }
  assert(false);
  return {};
}

// predicate: lexeme is a ending lexeme
optional<Datum> Reader::Stack::endStackFront(Lexeme lexeme) {
  if (empty()) {
    return DK_Error;
  }
  switch (frontKind()) {
  case LK_LeftParentheses:       // (
  case LK_PoundParentheses:      // #(
  case LK_Pound_vu8_Parentheses: // #vu8,#u8(
    if (lexeme.kind != LK_RightParentheses) {
      return DK_Error;
    }
    return combineStackFront(takeFront());
  case LK_LeftBracket: // [
    if (lexeme.kind != LK_RightBracket) {
      return DK_Error;
    }
    return combineStackFront(takeFront());
  default:
    return DK_Error;
  }
  assert(false);
  return DK_Error;
}

std::optional<Datum> Reader::read(LexerPosition &pos) {
  while (pos) {
    auto lexeme = lexer->lex(pos);
    if (!lexeme) {
      return {};
    }
    switch (lexeme->kind) {
    case LK_Period:                // .
    case LK_Identidier:
    case LK_Boolean:
    case LK_Number:
    case LK_Character:
    case LK_String: {
      if (auto ret = stack.combineStackFront(lexeme.value())) {
        if (ret->getKind() == DK_Error) {
          return DK_Error;
        } else {
          return ret;
        }
      } else {
        continue;
      }
    }
    case LK_RightParentheses: // )
    case LK_RightBracket:     // ]
      if (auto ret = stack.endStackFront(lexeme.value())) {
        if (ret->getKind() == DK_Error) {
          return DK_Error;
        } else {
          return ret;
        }
      } else {
        continue;
      }
    case LK_LeftParentheses:       // (
    case LK_LeftBracket:           // [
    case LK_PoundParentheses:      // #(
    case LK_Pound_vu8_Parentheses: // #vu8,#u8(
    case LK_Apostrophe:            // '
    case LK_BackQuote:             // `
    case LK_Comma:                 // ,
    case LK_CommaAt:               // ,@
    case LK_PoundApostrophe:       // #'
    case LK_PoundBackQuote:        // #`
    case LK_PoundComma:            // #,
    case LK_PoundCommaAt:          // #,@
    case LK_PoundSemicolon:        // #;
      stack.push(lexeme->kind);
      continue;
    default:
      assert(false);
    }
  }
  return {};
}
} // namespace scheme2llvm

