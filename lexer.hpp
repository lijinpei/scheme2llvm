#ifndef SCHEME2LLVM_LEXER_HPP
#define SCHEME2LLVM_LEXER_HPP

#include "config.hpp"
#include "number.hpp"
#include "utf8.hpp"

#include "llvm/Support/raw_ostream.h"
#include <math.h>
#include <memory>
#include <optional>
#include <string>
#include <unordered_set>
// TODO: add support for r7rs label
namespace scheme2llvm {
struct Lexeme;
struct LexerPosition;
class Lexer;
void u32string_print(const std::u32string &str, llvm::raw_ostream &os);
// inline hex escape can introduce unicode identifier
using IdentifierRepresentation = std::unordered_set<std::u32string>::iterator;
using StringRepresentation = std::unordered_set<std::u32string>::iterator;

enum LexemeKind {
  LK_Identidier,
  LK_Boolean,
  LK_Number,
  LK_Character,
  LK_String,
  LK_LeftParentheses,       // (
  LK_RightParentheses,      // )
  LK_LeftBracket,           // [
  LK_RightBracket,          // ]
  LK_PoundParentheses,      // #(
  LK_Pound_vu8_Parentheses, // #vu8,#u8(
  LK_PoundSemicolon,  // #; we do not handle datum comment on lexeme syntax
                      // level
  LK_Apostrophe,      // '
  LK_BackQuote,       // `
  LK_Comma,           // ,
  LK_CommaAt,         // ,@
  LK_Period,          // .
  LK_PoundApostrophe, // #'
  LK_PoundBackQuote,  // #`
  LK_PoundComma,      // #,
  LK_PoundCommaAt,    // #,@
  LK_Count,           // total kinds of legal lemxes
  LK_IllegalInput,    // the input is not valid
  LK_InternalError,   // some internal invariant doesn't hold
  LK_Blank            // got nothing other than blank
};

class LexerPosition {
  utf8_ptr p;
  int line_count;

public:
  LexerPosition(char *p_, char *pe_, int line_count_ = 0)
      : p(p_, pe_), line_count(line_count_) {}
  char *pos() { return p.pos(); }
  char *pos_end() { return p.pos_end(); }
  bool set_pos(char *np) { return p.set_pos(np); }
  auto operator++(int) { return p++; }
  void inc_line(int n = 1) { line_count += n; }
  operator bool() { return p.operator bool(); }
};

struct Lexeme {
  LexemeKind kind;
  union {
    char32_t character;
    bool boolean;
    IdentifierRepresentation identifier;
    StringRepresentation string;
    ComplexNumber number;
  };

  Lexeme(LexemeKind lk) : kind(lk) {}
  Lexeme(char32_t ch) : kind(LK_Character), character(ch) {}
  Lexeme(bool b) : kind(LK_Boolean), boolean(b) {}
  Lexeme(ComplexNumber n) : kind(LK_Number), number(n) {}
  template <class T> Lexeme(LexemeKind lk, const T &v) {
    kind = lk;
    if constexpr (std::is_same_v<T, char32_t>) {
      assert(lk == LK_Character);
      character = v;
    } else if constexpr (std::is_same_v<T, bool>) {
      assert(lk == LK_Boolean);
      boolean = v;
    } else if constexpr (std::is_same_v<T, IdentifierRepresentation>) {
      if (lk == LK_Identidier) {
        identifier = v;
      } else if (lk == LK_String) {
        string = v;
      } else {
        assert(false);
      }
    } else if constexpr (std::is_same_v<T, ComplexNumber>) {
      assert(lk == LK_Number);
      number = v;
    } else {
      assert(false);
    }
  }
  Lexeme(const Lexeme &rhs) {
    if (rhs.kind != LK_Number) {
      memcpy(this, &rhs, sizeof(Lexeme));
    } else {
      kind = LK_Number;
      new (&number) ComplexNumber(rhs.number);
    }
  }
  Lexeme &operator=(const Lexeme &rhs) {
    this->~Lexeme();
    if (rhs.kind != LK_Number) {
      memcpy(this, &rhs, sizeof(Lexeme));
    } else {
      kind = LK_Number;
      new (&number) ComplexNumber(rhs.number);
    }
    return *this;
  }
  ~Lexeme() {
    if (kind == LK_Number) {
      number.~ComplexNumber();
    }
  }
  template <class T> T get() {
    if constexpr (std::is_same_v<T, char32_t>) {
      assert(kind == LK_Character);
      return character;
    } else if constexpr (std::is_same_v<T, bool>) {
      assert(kind == LK_Boolean);
      return boolean;
    } else if constexpr (std::is_same_v<T, IdentifierRepresentation>) {
      if (kind == LK_Identidier) {
        return identifier;
      } else if (kind == LK_String) {
        return string;
      } else {
        assert(false);
      }
    } else if constexpr (std::is_same_v<T, ComplexNumber>) {
      assert(kind == LK_Number);
      return number;
    } else {
      assert(false);
      return T();
    }
  }
  void print(llvm::raw_ostream &) const;
};

class Lexer {
  std::unordered_set<std::u32string> identifier_table;
  std::unordered_set<std::u32string> string_table;

public:
  std::optional<Lexeme> lex(LexerPosition &);

  bool reset() {
    identifier_table.clear();
    string_table.clear();
    return true;
  }

  IdentifierRepresentation addIdentifier(const std::u32string &id) {
    return identifier_table.insert(id).first;
  }

  StringRepresentation addString(const std::u32string &str) {
    return string_table.insert(str).first;
  }

  Lexeme identifierLexeme(const std::u32string &id) {
    return {LK_Identidier, addIdentifier(id)};
  }

  std::optional<Lexeme> identifierLexeme(std::optional<std::u32string> &&id) {
    if (id) {
      return identifierLexeme(id.value());
    } else {
      return {};
    }
  }

  Lexeme stringLexeme(const std::u32string &str) {
    return {LK_String, addString(str)};
  }

  std::optional<Lexeme> stringLexeme(std::optional<std::u32string> &&str) {
    if (str) {
      return stringLexeme(str.value());
    } else {
      return {};
    }
  }
};
} // namespace scheme2llvm

#endif // SCHEME2LLVM_LEXER_HPP
