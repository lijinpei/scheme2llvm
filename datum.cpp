#include "datum.hpp"

#include "llvm/Support/raw_ostream.h"

namespace scheme2llvm {
Abbreviation::~Abbreviation() {
  if (kind != AbbrK_Null) {
    sub_datum->~Datum();
    deallocate(sub_datum, 1);
  }
}
Abbreviation::Abbreviation(AbbreviationKind abbr_k, Datum &&d) : kind(abbr_k) {
  sub_datum = allocate<Datum>(1);
  assert(sub_datum);
  new (sub_datum) Datum(std::move(d));
}
void Abbreviation::write(llvm::raw_ostream &os) const {
  switch (kind) {
  case (AbbrK_Apostrophe):
    os << '\'';
    break;
  case (AbbrK_BackQuote):
    os << '`';
    break;
  case (AbbrK_Comma):
    os << ',';
    break;
  case (AbbrK_CommaAt):
    os << ",@";
    break;
  case (AbbrK_PoundApostrophe):
    os << "#\'";
    break;
  case (AbbrK_PoundBackQuote):
    os << "#`";
    break;
  case (AbbrK_PoundComma):
    os << "#,";
    break;
  case (AbbrK_PoundCommaAt):
    os << "#,@";
    break;
  default:
    assert(false);
  }
  sub_datum->write(os);
}
void DatumList::write(llvm::raw_ostream &os) const {
  os << '(' << static_cast<const internal::MoveOnlyList<Datum> &>(*this) << ')';
}
void DatumImproperList::write(llvm::raw_ostream &os) const {
  assert(size >= 2);
  os << '(';
  for (size_t i = 0; i + 1 < size; ++i) {
    os << begin[i] << ' ';
  }
  os << ". ";
  os << begin[size - 1] << ")";
}
void DatumVector::write(llvm::raw_ostream &os) const {
  os << "#(" << static_cast<const internal::MoveOnlyList<Datum> &>(*this)
     << ')';
}
void ByteVector::write(llvm::raw_ostream &os) const {
  os << "#vu8(";
  for (size_t i = 0; i < size; ++i) {
    os << begin[i];
    if (i + 1 != size) {
      os << ' ';
    }
  }
  os << ')';
}
void Datum::write(llvm::raw_ostream &os) const {
  switch (kind) {
  case DK_Boolean:
    if (boolean) {
      os << "#t";
    } else {
      os << "#f";
    }
    break;
  case DK_Character:
    os << "\\#x";
    os.write_hex(character);
    break;
  case DK_Number:
    number.write(os);
    break;
  case DK_String:
    os << '"';
    u32string_print(*string, os);
    os << '"';
    break;
  case DK_Symbol:
    u32string_print(*symbol, os);
    break;
  case DK_List:
    os << list;
    break;
  case DK_ImproperList:
    os << impr_list;
    break;
  case DK_Vector:
    os << vector;
    break;
  case DK_ByteVector:
    os << byte_vector;
    break;
  case DK_Abbreviation:
    os << abbr;
    break;
  case DK_Null:
  case DK_Error:
  case DK_Period:
  case DK_Count:
  default:
    llvm::errs() << "kind: " << kind << '\n';
    assert(false);
  }
}
} // namespace scheme2llvm

