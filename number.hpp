#ifndef SCHEME2LLVM_NUMBER_HPP
#define SCHEME2LLVM_NUMBER_HPP

#include "config.hpp"

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/APSInt.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/raw_ostream.h"
#include <cassert>

namespace scheme2llvm {
using llvm::APFloat;
using llvm::APSInt;

enum RealNumberKind { RNK_Null, RNK_Integer, RNK_Rational, RNK_Float };

struct RealNumber {
  RealNumberKind rnk;
  union {
    std::nullptr_t null;
    APSInt integer;
    struct {
      APSInt denominator, numerator;
    } rational;
    APFloat real;
  };
  RealNumber() : rnk(RNK_Null), null(nullptr) {}
  RealNumber(const APSInt &i) : rnk(RNK_Integer), integer(i) {}
  RealNumber(const APSInt &den, const APSInt &num)
      : rnk(RNK_Rational), rational{den, num} {}
  RealNumber(const APFloat &f) : rnk(RNK_Float), real(f) {}
  RealNumber(const RealNumber &rhs) {
    rnk = rhs.rnk;
    switch (rhs.rnk) {
    case RNK_Null:
      null = nullptr;
      break;
    case RNK_Integer:
      new (&integer) APSInt(rhs.integer);
      break;
    case RNK_Rational:
      new (&rational.denominator) APSInt(rhs.rational.denominator);
      new (&rational.numerator) APSInt(rhs.rational.numerator);
      break;
    case RNK_Float:
      new (&real) APFloat(rhs.real);
      break;
    default:
      assert(false);
    }
  }
  ~RealNumber() { this->destroy(); }

  RealNumber &operator=(const RealNumber &rhs) {
    this->destroy();
    rnk = rhs.rnk;
    switch (rhs.rnk) {
    case RNK_Null:
      null = nullptr;
      break;
    case RNK_Integer:
      new (&integer) APSInt{rhs.integer};
      break;
    case RNK_Rational:
      new (&rational) decltype(rational)(rhs.rational);
      break;
    case RNK_Float:
      new (&real) APFloat(rhs.real);
      break;
    default:
      assert(false);
    }
    return *this;
  }

  bool verifyRNK() const {
    return rnk == RNK_Null || rnk == RNK_Float || rnk == RNK_Rational ||
           rnk == RNK_Integer;
  }

  void destroy() {
    if constexpr (scheme2llvm_debug) {
      assert(verifyRNK());
    }
    switch (rnk) {
    case RNK_Null:
      break;
    case RNK_Integer:
      integer.~APSInt();
      break;
    case RNK_Rational:
      rational.denominator.~APSInt();
      rational.numerator.~APSInt();
      break;
    case RNK_Float:
      real.~APFloat();
      break;
    default:
      assert(false);
    }
  }

  template <RealNumberKind rnk1> bool promoteTo() {
    static_assert(rnk1 == RNK_Float || rnk1 == RNK_Rational ||
                  rnk1 == RNK_Integer);
    if (rnk == RNK_Null) {
      return false;
    }
    assert(rnk == RNK_Float || rnk == RNK_Rational || rnk == RNK_Integer);
    if (rnk1 < rnk) {
      return false;
    }
    if (rnk1 == rnk) {
      return true;
    }
    if constexpr (rnk1 == RNK_Float) {
      if (rnk == RNK_Rational) {
        APFloat v1(APFloat::IEEEdouble(), rational.numerator);
        APFloat v2(APFloat::IEEEdouble(), rational.denominator);
        if (v1.divide(v2, APFloat::rmTowardZero) == APFloat::opOK) {
          *this = RealNumber(v1);
          return true;
        } else {
          return false;
        }
      } else if (rnk == RNK_Integer) {
        APFloat v(APFloat::IEEEdouble(), integer);
        *this = RealNumber(v);
        return true;
      }
    } else if constexpr (rnk1 == RNK_Rational) {
      APSInt v1(integer);
      *this = RealNumber(v1, APSInt::get(1));
      return true;
    }
    assert(false);
    return false;
  }

  bool promoteTo(RealNumberKind rnk1) {
    switch (rnk1) {
    case RNK_Integer:
      return this->promoteTo<RNK_Integer>();
    case RNK_Rational:
      return this->promoteTo<RNK_Rational>();
    case RNK_Float:
      return this->promoteTo<RNK_Float>();
    case RNK_Null:
      return false;
    default:
      assert(false);
      return false;
    }
  }

  static RealNumberKind promote(RealNumber &r1, RealNumber &r2) {
    if constexpr (scheme2llvm_debug) {
      assert(r1.verifyRNK() && r2.verifyRNK());
    }
    auto rnk1 = std::max(r1.rnk, r2.rnk);
    if (r1.promoteTo(rnk1) && r2.promoteTo(rnk1)) {
      return rnk1;
    } else {
      assert(false);
      return RealNumberKind(-1);
    }
  }

  bool isZero() const {
    if (rnk == RNK_Null) {
      return true;
    }
    switch (rnk) {
    case RNK_Integer:
      return llvm::APSInt::isSameValue(integer, llvm::APSInt::get(0));
    case RNK_Rational:
      return llvm::APSInt::isSameValue(rational.numerator,
                                       llvm::APSInt::get(0)) &&
             !llvm::APSInt::isSameValue(rational.denominator,
                                        llvm::APSInt::get(0));
    case RNK_Float:
      return real.isZero();
    default:
      assert(false);
    }
  }

  bool isPositive() const {
    if (rnk == RNK_Null) {
      return true;
    }
    switch (rnk) {
    case RNK_Integer:
      return integer.isStrictlyPositive();
    case RNK_Rational: {
      if (llvm::APSInt::isSameValue(rational.numerator, llvm::APSInt::get(0)) ||
          llvm::APSInt::isSameValue(rational.denominator,
                                    llvm::APSInt::get(0))) {
        return false;
      }
      bool b1 = rational.numerator.isNegative();
      bool b2 = rational.numerator.isNegative();
      return !(b1 ^ b2);
    }
    case RNK_Float:
      return !real.isZero() && !real.isNegative();
    default:
      assert(false);
    }
  }

  bool isNegative() const {
    if (rnk == RNK_Null) {
      return true;
    }
    switch (rnk) {
    case RNK_Integer:
      return integer.isNegative();
    case RNK_Rational: {
      if (llvm::APSInt::isSameValue(rational.numerator, llvm::APSInt::get(0)) ||
          llvm::APSInt::isSameValue(rational.denominator,
                                    llvm::APSInt::get(0))) {
        return false;
      }
      bool b1 = rational.numerator.isNegative();
      bool b2 = rational.numerator.isNegative();
      return (b1 ^ b2);
    }
    case RNK_Float:
      return real.isNegative();
    default:
      assert(false);
    }
    assert(false);
    return false;
  }

  bool isNonPositive() const {
    if (rnk == RNK_Null) {
      return true;
    }
    return !isPositive();
  }

  bool isNonNegative() const {
    if (rnk == RNK_Null) {
      return true;
    }
    return !isNegative();
  }

  void printValue(llvm::raw_ostream &os) const {
    switch (rnk) {
    case RNK_Null:
      os << "null";
      break;
    case RNK_Integer:
      integer.print(os, true);
      break;
    case RNK_Rational:
      rational.numerator.print(os, true);
      os << '/';
      rational.denominator.print(os, true);
      break;
    case RNK_Float: {
      // I don't know why APFlot.print() append a "\n"
      llvm::SmallVector<char, 16> Buffer;
      real.toString(Buffer);
      os << Buffer;
    } break;
    default:
      assert(false);
    }
  }

  void printKind(llvm::raw_ostream &os) const {
    switch (rnk) {
    case RNK_Null:
      os << "null_kind";
      break;
    case RNK_Integer:
      os << "Integer";
      break;
    case RNK_Rational:
      os << "Rational";
      break;
    case RNK_Float:
      os << "Float";
      break;
    default:
      assert(false);
    }
  }

  void write(llvm::raw_ostream &os) const { printValue(os); }
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const RealNumber &r) {
  r.write(os);
  return os;
}

enum ComplexNumberKind {
  CNK_Integer = RNK_Integer,
  CNK_Rational = RNK_Rational,
  CNK_Float = RNK_Float,
  CNK_Real, // A real number doesn't have a imaginary part
};

struct ComplexNumber {
  RealNumber real_part, imag_part;
  ComplexNumberKind cnk;
  ComplexNumber()
      : real_part(RealNumber(APSInt::get(0))),
        imag_part(RealNumber(APSInt::get(0))), cnk(CNK_Integer) {}
  ComplexNumber(const RealNumber &r, const RealNumber &i)
      : real_part(r), imag_part(i) {
    if ((cnk = ComplexNumberKind(RealNumber::promote(real_part, imag_part))) <
        0) {
      assert(false);
    }
  }
  ComplexNumber(const RealNumber &r) : real_part(r), cnk(CNK_Real) {}
  static ComplexNumber polarCoordinate(RealNumber v1, RealNumber v2) {
    ComplexNumber ret;
    ret.cnk = CNK_Float;
    v1.promoteTo<RNK_Float>();
    v2.promoteTo<RNK_Float>();
    double d1 = v1.real.convertToDouble(), d2 = v2.real.convertToDouble();
    double d3, d4;
    sincos(d2, &d4, &d3);
    d3 *= d1;
    d4 *= d1;
    ret.real_part = APFloat(d3);
    ret.imag_part = APFloat(d4);
    return ret;
  }

  void printValue(llvm::raw_ostream &os) const {
    switch (cnk) {
    case CNK_Integer:
    case CNK_Rational:
    case CNK_Float:
      real_part.printValue(os);
      if (imag_part.isNonNegative()) {
        os << '+';
      }
      imag_part.printValue(os);
      os << 'i';
      break;
    case CNK_Real:
      real_part.printValue(os);
      break;
    default:
      assert(false);
    }
  }

  void printKind(llvm::raw_ostream &os) const {
    switch (cnk) {
    case CNK_Integer:
      os << "Complex/Integer";
      break;
    case CNK_Rational:
      os << "Complex/Rational";
      break;
    case CNK_Float:
      os << "Complex/Float";
      break;
    case CNK_Real:
      os << "Complex/Real/";
      real_part.printKind(os);
      break;
    default:
      assert(false);
    }
  }

  void write(llvm::raw_ostream &os) const { printValue(os); }
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const ComplexNumber &c) {
  c.write(os);
  return os;
}

} // namespace scheme2llvm

#endif // SCHEME2LLVM_NUMBER_HPP
