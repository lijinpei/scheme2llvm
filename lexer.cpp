#include "lexer.hpp"
#include "utf8.hpp"

#include "unicode/uchar.h"
#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/APSInt.h"
#include "llvm/Support/raw_ostream.h"
#include <cassert>
#include <cmath>
#include <codecvt>
#include <iostream>
#include <limits>
#include <locale>
#include <optional>
#include <string>
#include <type_traits>

namespace scheme2llvm {
using std::optional;
using std::u32string;

void u32string_print(const u32string &str, llvm::raw_ostream &os) {
  std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
  os << conv.to_bytes(str);
}

namespace {

constexpr char32_t line_feed = U'\U0000000A', carriage_return = U'\U0000000D',
                   unicode_max = U'\U0010FFFF';

inline bool is_intraline_whitespace(char32_t c) {
  return c == U'\t' || c == U' ';
}

inline bool is_special_initial(char32_t c) {
  switch (c) {
  case U'!':
  case U'$':
  case U'%':
  case U'&':
  case U'*':
  case U'/':
  case U':':
  case U'<':
  case U'=':
  case U'>':
  case U'?':
  case U'@':
  case U'^':
  case U'_':
  case U'~':
    return true;
  default:
    return false;
  }
}

// is a valid initial for a normal identifier
template <bool McMeNd = false> inline bool is_initial(char32_t c) {
  if (c >= 'a' && c <= 'z') {
    return true;
  }
  if (c >= 'A' && c <= 'Z') {
    return true;
  }
  if (is_special_initial(c)) {
    return true;
  }
  if (c == U'\U0000200C' || c == U'\U0000200D') {
    return true;
  }
  auto gc = u_charType(c);
  switch (gc) {
  case U_UPPERCASE_LETTER:      // Lu
  case U_LOWERCASE_LETTER:      // Ll
  case U_TITLECASE_LETTER:      // Lt
  case U_MODIFIER_LETTER:       // Lm
  case U_OTHER_LETTER:          // Lo
  case U_NON_SPACING_MARK:      // Mn
  case U_LETTER_NUMBER:         // Nl
  case U_OTHER_NUMBER:          // No
  case U_DASH_PUNCTUATION:      // Pd
  case U_CONNECTOR_PUNCTUATION: // Pc
  case U_OTHER_PUNCTUATION:     // Po
  case U_CURRENCY_SYMBOL:       // Sc
  case U_MATH_SYMBOL:           // Sm
  case U_MODIFIER_SYMBOL:       // Sk
  case U_OTHER_SYMBOL:          // So
  case U_PRIVATE_USE_CHAR:      // Co
    return true;
  case U_COMBINING_SPACING_MARK: // Mc
  case U_ENCLOSING_MARK:         // Me
  case U_DECIMAL_DIGIT_NUMBER:   // Nd
    return McMeNd;
  default:
    return false;
  }
}

inline bool is_subsequent(char32_t c) {
  if (c >= U'0' && c <= U'9') {
    return true;
  }
  // U'@' is a special initial and a special subsequent
  switch (c) {
  case U'+':
  case U'-':
  case U'.':
  case U'@':
    return true;
  default:
    // now count Mc Me Nd as initial
    return is_initial<true>(c);
  }
}

inline bool is_delimiter(char32_t c) {
  switch (c) {
  case U'\0':
  case U'(':
  case U')':
  case U'[':
  case U']':
  case U'"':
  case U';':
  case U'\n':
  case U'\r':
    return true;
  default:
    return is_intraline_whitespace(c);
  }
}

/* predicate: non-subsequents part of the peculiar identifier is in string
 * this function complete the identifier by append the subsequents and check the
 * identifier is properly delimited this function doesn't distinguish +inf.0, +i
 * etc(inf/nan/i) notice that +.0, .0, +0, .a etc doesn't conform to peculiar
 * identifier
 */
bool complete_peculiar_identifier(LexerPosition &pos, u32string &string) {
  for (; pos;) {
    char32_t c = pos++;
    if (is_subsequent(c)) {
      string.push_back(c);
    } else {
      return is_delimiter(c);
    }
  }
  return true;
}
/* lex a sequence of hex value as unicode point, not including the final ';'
 */
optional<char32_t> lex_hex_scalar(LexerPosition &pos) {
  bool flag = false;
  int64_t v = 0;
  for (; pos;) {
    LexerPosition p0 = pos;
    char32_t c = pos++;
    if (c >= U'0' && c <= U'9') {
      v = (v << 4) + c - U'0';
      flag = true;
    } else if (c >= U'a' && c <= U'f') {
      v = (v << 4) + c - U'a' + 10;
      flag = true;
    } else if (c >= U'A' && c <= U'F') {
      v = (v << 4) + c - U'A' + 10;
      flag = true;
    } else {
      if (flag) {
        pos = p0;
        return v;
      } else {
        return {};
      }
    }
    if (v > unicode_max) {
      return {};
    }
  }
  assert(false);
  return {};
}
/* lex a sequence of hex value as unicode point, including the final ';'
 * predicate: "\x" has been seen
 */
optional<char32_t> get_inline_hex_escape(LexerPosition &pos) {
  auto ret = lex_hex_scalar(pos);
  if (ret && pos++ == U';') {
    return ret;
  } else {
    return {};
  }
}

/* predicate: *(pos.p - 1) == '"'
 */
optional<ComplexNumber> get_number(LexerPosition &pos);
/* predicate: *(pos.p - 1) == '|'
 */
optional<u32string> get_vertical_line_identifier(LexerPosition &pos);
/* lex a non-vertial-line, non-peculiar identifier
 */
optional<u32string> get_normal_identifier(LexerPosition &pos);

// non commnet whitespace
template <bool eat_intraline = true>
bool try_eat_whitespace(LexerPosition &pos) {
  LexerPosition p0 = pos;
  char32_t c0 = pos++;

  if constexpr (eat_intraline) {
    if (is_intraline_whitespace(c0)) {
      return true;
    }
  }

  switch (c0) {
  case carriage_return: {
    LexerPosition pos1 = pos;
    char32_t c1 = pos++;
    if (c1 == line_feed) {
      pos.inc_line();
      return true;
    } else {
      pos = pos1;
      pos.inc_line();
      return true;
    }
  }
  case line_feed: {
    pos.inc_line();
    return true;
  }
  default:
    pos = p0;
    return false;
  }
  assert(false);
  return false;
}

inline bool peek_delimiter(LexerPosition pos) { return is_delimiter(pos++); }

inline bool is_digit(char32_t c) { return c >= U'0' && c <= U'9'; }

inline bool is_sign_subsequent(char32_t c) {
  return c == U'+' || c == U'-' || c == U'@' || is_initial(c);
}

inline bool is_dot_subsequent(char32_t c) {
  return c == U'.' || is_sign_subsequent(c);
}

// predicate: "#\" has been seen
optional<char32_t> lex_character(LexerPosition &pos) {
  LexerPosition p0 = pos;
  char32_t c = pos++;
  if (peek_delimiter(pos)) {
    return c;
  }
  if (c == U'x' || c == U'X' || c == U'u' || c == U'U') {
    // NOTE: #\U #\u is my extension, make sure it doesn't conflict with other
    // sequences
    auto ret = lex_hex_scalar(pos);
    if (ret && peek_delimiter(pos)) {
      return ret;
    } else {
      return {};
    }
  } else {
    pos = p0;
    auto ret = get_normal_identifier(pos);
    if (!ret) {
      return {};
    }
    using PairT = std::pair<char32_t, u32string>;
    std::vector<PairT> map{
        {U'\0', U"null"},      {U'\0', U"nul"},      {U'\a', U"alarm"},
        {U'\b', U"backspace"}, {U'\t', U"tab"},      {U'\n', U"newline"},
        {U'\n', U"linefeed"},  {U'\v', U"vtab"},     {U'\f', U"page"},
        {U'\r', U"return"},    {U'\x1b', U"escape"}, {U'\x1b', U"esc"},
        {U' ', U"space"},      {U'\x7f', U"delete"}};
    // TODO: add all ascii abbreviation
    auto find = std::find_if(map.begin(), map.end(), [&](const PairT &p) {
      return ret.value() == p.second;
    });
    if (find != map.end()) {
      return find->first;
    } else {
      return {};
    }
  }
  assert(false);
  return {};
}

bool transform_mnemonic(char32_t &c) {
  using PairT = std::pair<char32_t, char32_t>;
  std::vector<PairT> map{{U'\a', U'a'},   {U'\b', U'b'},
                         {U'\x1b', U'e'}, // This is my extension, not R7RS
                         {U'\n', U'n'},   {U'\r', U'r'},
                         {U'\t', U'r'},   {U'\\', U'\\'},
                         {U'|', U'|'},    {U'"', U'"'}};
  auto find = std::find_if(map.begin(), map.end(),
                           [&](const PairT &p) { return c == p.second; });
  if (find != map.end()) {
    c = find->first;
    return true;
  } else {
    return false;
  }
}

/* this function tries to eat
 * <intra line whitespace>* <line ending> <intra line whitespace>*
 */
bool try_eat_line_ending_and_stuff(LexerPosition &pos) {
  auto eat_intraline_whitespaces = [&]() {
    for (; pos;) {
      if (is_intraline_whitespace(pos++)) {
        continue;
      } else {
        return;
      }
    }
  };
  eat_intraline_whitespaces();
  if (try_eat_whitespace<false>(pos)) {
    eat_intraline_whitespaces();
    return true;
  } else {
    return false;
  }
}

constexpr char32_t string_end = -1;
constexpr char32_t invalid_code_point = unicode_max + 1;
char32_t get_string_element(LexerPosition &pos) {
again:
  char32_t c = pos++;
  if (c == U'"') {
    return string_end;
  }
  if (c == U'\\') {
    LexerPosition pos1 = pos;
    char32_t c1 = pos++;
    if (c1 == U'x') {
      auto ret = get_inline_hex_escape(pos);
      return ret ? ret.value() : invalid_code_point;
    }
    if (transform_mnemonic(c1)) {
      return c1;
    }
    if (try_eat_line_ending_and_stuff(pos1)) {
      pos = pos1;
      goto again;
    }
    return invalid_code_point;
  }
  return c;
}

// predicate: the openning U'"' has been seen
// postcondition: *(p - 1) is a terminating U'"'
optional<u32string> lex_string(LexerPosition &pos) {
  u32string ret;
  while (pos) {
    char32_t c = get_string_element(pos);
    if (c == string_end) {
      return ret;
    } else if (c <= unicode_max) {
      ret.push_back(c);
    } else {
      return {};
    }
  }
  assert(false);
  return {};
}

// predicate: *(p - 1) == U'|', *(p - 2) == U'#'
bool eat_nested_comment(LexerPosition &pos) {
  int nest_count = 1;
  int new_line = 0;
  char32_t c = pos++;
  LexerPosition p1 = pos, p2 = pos;
  char32_t c1 = p2++;
  auto forward2 = [&]() {
    c = p2++;
    p1 = p2;
    c1 = p2++;
  };
  auto forward1 = [&]() {
    c = c1;
    p1 = p2;
    c1 = p2++;
  };
  while (pos) {
    if (c == U'#') {
      if (c1 == U'|') {
        ++nest_count;
        forward2();
        continue;
      } else {
        forward1();
        continue;
      }
    }
    if (c == U'|') {
      if (c1 == U'#') {
        --nest_count;
        if (!nest_count) {
          pos = p2;
          pos.inc_line(new_line);
          return true;
        } else {
          forward2();
          continue;
        }
      } else {
        forward1();
        continue;
      }
    }
    if (c == carriage_return) {
      ++new_line;
      if (c1 == line_feed) {
        forward2();
        continue;
      } else {
        forward1();
        continue;
      }
    }
    if (c == line_feed) {
      ++new_line;
      forward1();
      continue;
    }
    forward1();
  }
  return false;
}

bool is_digit(char c, int base) {
  assert(base == 2 || base == 8 || base == 10 || base == 16);
  if (c >= '0' && c < '2') {
    // return base >= 2;
    return true;
  }
  if (c >= '2' && c < '8') {
    return base >= 8;
  }
  if (c >= '8' && c <= '9') {
    return base >= 10;
  }
  return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) && (base == 16);
}

optional<APFloat> eat_inf(LexerPosition &pos, bool positive) {
  char *p = pos.pos(), *pe = pos.pos_end();
  if (p + 5 >= pe) {
    return {};
  }
  std::string string(p, 5);
  if (string == "inf.0") {
    pos.set_pos(p + 5);
    return APFloat::getInf(llvm::APFloat::IEEEdouble(), !positive);
  } else {
    return {};
  }
}

optional<APFloat> eat_nan(LexerPosition &pos, bool positive) {
  char *p = pos.pos(), *pe = pos.pos_end();
  if (p + 5 >= pe) {
    return {};
  }
  std::string string(p, 5);
  if (string == "nan.0") {
    pos.set_pos(p + 5);
    return APFloat::getQNaN(llvm::APFloat::IEEEdouble(), !positive);
  } else {
    return {};
  }
}

// lex a decimal real number start with a '.'
optional<APFloat> get_decimal_real(LexerPosition &pos) {
  LexerPosition p = pos, p0 = p;
  p++;
  for (; p;) {
    char32_t c = p++;
    if (!is_digit(c)) {
      break;
    }
  }
  size_t s = p.pos() - p0.pos();
  if (1 == s) {
    return {};
  }
  llvm::StringRef str_ref(p0.pos(), s);
  pos = p;
  return APFloat(APFloat::IEEEdouble(), str_ref);
}

optional<APSInt> get_R_integer(LexerPosition &pos, int base) {
  // we need to find the end of the integer ourself
  char *p = pos.pos(), *pe = pos.pos_end(), *p0 = p;
  if ((p < pe) && (*p == '-' || *p == '+')) {
    ++p;
  }
  for (; p < pe; ++p) {
    if (!is_digit(*p, base)) {
      break;
    }
  }
  if (p == p0) {
    return {};
  }
  llvm::StringRef str_ref(p0, p - p0);
  pos.set_pos(p);
  auto num_bits = APSInt::getBitsNeeded(str_ref, base);
  return APSInt(llvm::APInt(num_bits + 1, str_ref, base), false);
}

// this function don't check the retuned number is properly delimited
// it's get_number()'s resonsibility.
optional<RealNumber> get_R_real(LexerPosition &pos, int base) {
  // first try get +i/+inf.0/+nan.0
  LexerPosition p0 = pos;
  char32_t c = pos++;
  bool positive = true;
  switch (c) {
  case U'.':
    if (base != 10) {
      return {};
    }
    pos = p0;
    return get_decimal_real(pos);
  case U'-':
    positive = false;
    [[fallthrough]];
  case U'+': {
    LexerPosition p = pos;
    char32_t c1 = p++;
    if (c1 == U'i') {
      if (is_delimiter(p++)) {
        return APSInt::get(positive ? 1 : -1);
      } else {
        return eat_inf(pos, positive);
      }
    } else if (c1 == U'n') {
      return eat_nan(pos, positive);
    }
    [[fallthrough]];
  }
  default:
    break;
  }

  auto skip_digit = [base, &pos](LexerPosition &p, char32_t &c) {
    pos = p;
    for (; p;) {
      pos = p;
      c = p++;
      if (!is_digit(c, base)) {
        break;
      }
    }
  };
  LexerPosition p = pos;
  skip_digit(p, c);
  if (c == U'.') {
    if (base != 10) {
      return {};
    }
    skip_digit(p, c);
    llvm::StringRef str_ref(p0.pos(), p.pos() - p0.pos() - 1);
    return APFloat(APFloat::IEEEdouble(), str_ref);
  }
  if (c == U'/') {
    LexerPosition pn0 = p, pn = p;
    c = p++;
    if (c == U'+' || c == U'-') {
      pn = p;
    } else if (is_digit(c, base)) {
      pn = pn0;
    } else {
      return {};
    }
    skip_digit(pn, c);
    llvm::StringRef str_ref1(p0.pos(), p.pos() - p0.pos() - 1);
    llvm::StringRef str_ref2(pn0.pos(), pn.pos() - pn0.pos() - 1);
    auto num_bits1 = APSInt::getBitsNeeded(str_ref1, base) + 1;
    auto num_bits2 = APSInt::getBitsNeeded(str_ref2, base) + 1;
    return RealNumber(APSInt(llvm::APInt(num_bits1, str_ref1, base), false),
                      APSInt(llvm::APInt(num_bits2, str_ref2, base), false));
  }
  llvm::StringRef str_ref(p0.pos(), p.pos() - p0.pos() - 1);
  auto num_bits = APSInt::getBitsNeeded(str_ref, base) + 1;
  return APSInt(llvm::APInt(num_bits, str_ref, base), false);
}

// get_number should check the number is properly delimited
optional<ComplexNumber> get_number(LexerPosition &pos) {
  int base, exact, base1, exact1;
  auto get_prefix = [&pos](int &base, int &exact) {
    base = 0;
    exact = 0;
    LexerPosition p0 = pos;
    char32_t c = pos++;
    char32_t c1 = pos++;
    if (c != U'#') {
      pos = p0;
      return true;
    }
    switch (c1) {
    case U'b':
    case U'B':
      base = 2;
      break;
    case U'o':
    case U'O':
      base = 8;
      break;
    case U'd':
    case U'D':
      base = 10;
      break;
    case U'i':
    case U'I':
      exact = 1;
      break;
    case U'e':
    case U'E':
      exact = 2;
      break;
    default:
      return false;
    }
    return true;
  };
  if (!get_prefix(base, exact) || !get_prefix(base1, exact1) ||
      (base && base1) || (exact && exact1)) {
    return {};
  }
  base = base ? base : (base1 ? base : 10);
  exact = exact ? exact : (exact1 ? exact1 : 10);

  /* handle <complex R>
   * <complex R> =
   *   | <Real>
   *   | <Real> @ <Real>
   *   | <Real> <SReal> i
   *   | <Real>i
   * <Real> =
   *   | <UReal>
   *   | + <UReal>
   *   | - <Ureal>
   *   | +inf.0
   *   | -inf.0
   *   | +nan.0
   *   | -nan.0
   * <UReal> =
   *   | <UInteger>
   *   | <UInteger> / <UInteger>
   *   | <Decimal>
   */

  auto ret = get_R_real(pos, base);
  if (!ret) {
    return {};
  }
  LexerPosition p0 = pos;
  char32_t c = pos++;
  switch (c) {
  case U'i':
    return ComplexNumber(RealNumber(APSInt::get(0)), ret.value());
  case U'@': {
    auto ret1 = get_R_integer(pos, base);
    if (!ret1) {
      return {};
    }
    return ComplexNumber::polarCoordinate(ret.value(), ret1.value());
  }
  default:
    if (is_delimiter(c)) {
      pos = p0;
      return ret;
    } else {
      return {};
    }
  }
}

// predicate: pos is valid
optional<char32_t> get_symbol_element(LexerPosition &pos) {
  LexerPosition p0 = pos;
  char32_t c = pos++;
  if (!c) {
    return {};
  }
  if (c == U'|') {
    return invalid_code_point;
  }
  if (c == U'\\') {
    char32_t c1 = pos++;
    if (c1 == 'x') {
      return get_inline_hex_escape(pos);
    } else if (c1 == U'|') {
      return U'|';
    } else {
      if (transform_mnemonic(c1)) {
        return c1;
      } else {
        pos = p0;
        return {};
      }
    }
  }
  return c;
}

// predicate: *(p - 1) == U'|'
// getVerticalLineIdentifier should check identifier is properly delimited
optional<u32string> get_vertical_line_identifier(LexerPosition &pos) {
  LexerPosition p0 = pos;
  u32string str;
  while (pos) {
    auto ret = get_symbol_element(pos);
    if (!ret) {
      pos = p0;
      return {};
    }
    if (ret.value() > unicode_max) {
      goto out;
    }
    str.push_back(ret.value());
  }
out:
  if (str.size()) {
    return str;
  } else {
    return {};
  }
}

optional<u32string> get_normal_identifier(LexerPosition &pos) {
  u32string ret;
  LexerPosition p0 = pos;
  char32_t c = pos++;
  if (!is_initial(c)) {
    pos = p0;
    return {};
  }
  ret.push_back(c);
  while (pos) {
    LexerPosition p1 = pos;
    char32_t c = pos++;
    if (is_subsequent(c)) {
      ret.push_back(c);
    } else if (is_delimiter(c)) {
      pos = p1;
      return ret;
    } else {
      pos = p0;
      return {};
    }
  }
  return ret;
}

bool eatup_interlexeme_space(LexerPosition &pos) {
  for (; pos;) {
    LexerPosition p0 = pos;
    if (try_eat_whitespace(pos)) {
      continue;
    }
    char32_t c = pos++;
    if (c == U'#') {
      char32_t c1 = pos++;
      if (c1 == U'|') {
        if (!eat_nested_comment(pos)) {
          return false;
        } else {
          continue;
        }
      }
      pos = p0;
      return true;
    }
    if (c == U';') {
      while (pos) {
        c = pos++;
        if (c == carriage_return) {
          LexerPosition p = pos;
          char32_t c1 = p++;
          if (c1 == line_feed) {
            pos = p;
          }
          pos.inc_line();
          break;
        } else if (c == line_feed) {
          pos.inc_line();
          break;
        }
      }
      if (!pos) {
        return false;
      }
      continue;
    }
    pos = p0;
    return true;
  }
  return false;
}

// lex something start with a U'#', which is guaranteed to be not a
// comment(otherwise lex_interlexeme_space() would eat that).
// could get a character, literal boolean, a number start with a prefix, #u8,
// #(, label
optional<Lexeme> lex_pound(LexerPosition &pos, LexerPosition p0) {
  // cound be: #( #u8 #, #` #, #,@
  // #t #T #f #F #\<char>
  // #<redix> #<exactness>
  // internal error: #|(internal_error) #| #; #!r6rs
  LexerPosition p1 = pos;
  char32_t c1 = pos++;
  switch (c1) {
  case U'(':
    return LK_PoundParentheses;
  case U'`':
    return LK_PoundBackQuote;
  case U';':
    return LK_PoundSemicolon;
  case U',': {
    char32_t c2 = pos++;
    if (c2 == U'@') {
      return LK_PoundCommaAt;
    } else {
      pos = p1;
      return LK_PoundComma;
    }
  }
  case U'\\':
    return lex_character(pos);
  case U'i':
  case U'I':
  case U'e':
  case U'E':
  case U'b':
  case U'B':
  case U'o':
  case U'O':
  case U'd':
  case U'D':
  case U'x':
  case U'X': {
    pos = p0;
    return get_number(pos);
  }
  default: {
    pos = p1;
    auto ret = get_normal_identifier(pos);
    if (!ret) {
      return {};
    }
    u32string &str = ret.value();
    if (str == U"f" ||
        /*str == U"F" ||*/ str == U"false") {
      return Lexeme(LK_Boolean, false);
    } else if (str == U"t" || /* str == "T" ||*/
               str == U"true") {
      return Lexeme(LK_Boolean, true);
    } else if (str.compare(U"vu8") || str.compare(U"u8")) {
      if (pos++ == U'(') {
        return Lexeme(LK_Pound_vu8_Parentheses);
      } else {
        return {};
      }
    } else {
      return {};
    }
  }
  }
  assert(false);
  return {};
}

} // anonymous namespace

void Lexeme::print(llvm::raw_ostream &os) const {
  switch (kind) {
  case LK_Identidier:
    os << "<Identifier: length " << identifier->size() << " content ";
    u32string_print(*identifier, os);
    os << '>';
    break;
  case LK_Boolean:
    os << "<Boolean: " << boolean << '>';
    break;
  case LK_Number:
    os << '<';
    number.printKind(os);
    os << ": ";
    number.printValue(os);
    os << '>';
    break;
  case LK_Character:
    os << "<Character: " << character;
    os << '>';
    break;
  case LK_String:
    os << "<String: length " << string->size() << " content ";
    u32string_print(*string, os);
    os << '>';
    break;
  case LK_LeftParentheses:
    os << '(';
    break;
  case LK_RightParentheses:
    os << ')';
    break;
  case LK_LeftBracket:
    os << '[';
    break;
  case LK_RightBracket:
    os << ']';
    break;
  case LK_PoundParentheses:
    os << "#(";
    break;
  case LK_Pound_vu8_Parentheses:
    os << "#vu8(";
    break;
  case LK_PoundSemicolon:
    os << "#;";
    break;
  case LK_Apostrophe:
    os << '\'';
    break;
  case LK_BackQuote:
    os << '`';
    break;
  case LK_Comma:
    os << ',';
    break;
  case LK_CommaAt:
    os << ",@";
    break;
  case LK_Period:
    os << '.';
    break;
  case LK_PoundApostrophe:
    os << "#'";
    break;
  case LK_PoundBackQuote:
    os << "#`";
    break;
  case LK_PoundComma:
    os << "#,";
    break;
  case LK_PoundCommaAt:
    os << "#,@";
    break;
  default:
    assert(false);
  }
}
optional<Lexeme> Lexer::lex(LexerPosition &pos) {
  eatup_interlexeme_space(pos);
  if (!pos) {
    return {};
  }
  LexerPosition p0 = pos;
  char32_t c = pos++;
  switch (c) {
  case U'(':
    return LK_LeftParentheses;
  case U')':
    return LK_RightParentheses;
    /* This has been excluded from R7RS, but I decide to support it anyway
     */
  case U'[':
    return LK_LeftBracket;
  case U']':
    return LK_RightBracket;
  case U'\'':
    return LK_Apostrophe;
  case U'`':
    return LK_BackQuote;
  case U',': {
    LexerPosition p1 = pos;
    char32_t c1 = pos++;
    if (c1 == U'@') {
      return LK_CommaAt;
    } else {
      pos = p1;
      return LK_Comma;
    }
  }
  case U'.': {
    LexerPosition p1 = pos;
    char32_t c1 = pos++;
    if (is_dot_subsequent(c1)) {
      u32string string;
      string.push_back(U'.');
      string.push_back(c1);
      complete_peculiar_identifier(pos, string);
      // assert: sth begin with .<dot subsequent> can not be number
      return identifierLexeme(string);
    } else if (is_digit(c1)) {
      pos = p0;
      return get_number(pos);
    } else if (is_delimiter(c1)) {
      pos = p1;
      return LK_Period;
    } else {
      return {};
    }
  }
  case U'"': {
    return stringLexeme(lex_string(pos));
  }
  case U'+':
  case U'-': {
    char32_t c1 = pos++;
    if (is_delimiter(c1)) {
      u32string string;
      string.push_back(c);
      return identifierLexeme(string);
    } else if (is_sign_subsequent(c1)) {
      pos = p0;
      u32string string;
      string.push_back(c);
      string.push_back(c1);
      complete_peculiar_identifier(pos, string);
      return identifierLexeme(string);
    } else if (c1 == U'.') {
      char32_t c2 = pos++;
      if (is_dot_subsequent(c2)) {
        u32string string;
        string.push_back(c);
        string.push_back(U'.');
        string.push_back(c2);
        complete_peculiar_identifier(pos, string);
        // assert: sth begin with +.<dot subsequent> can not be number
        return identifierLexeme(string);
      } else {
        return {};
      }
    } else if (is_digit(c1)) {
      pos = p0;
      return get_number(pos);
    } else {
      return {};
    }
  }
  case U'#': {
    return lex_pound(pos, p0);
  }
  case U'|':
    return identifierLexeme(get_vertical_line_identifier(pos));
  case U';':
    llvm::outs() << "LK_InternalError\n";
    return LK_InternalError;
  default:
    if (is_digit(c)) {
      pos = p0;
      return get_number(pos);
    } else {
      // try lex a unicode identifier
      pos = p0;
      return identifierLexeme(get_normal_identifier(pos));
    }
  }
  assert(false);
  return {};
}

} // namespace scheme2llvm

