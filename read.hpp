#ifndef SCHEME2LLVM_READ
#define SCHEME2LLVM_READ

/* This file the define read form(a.k.a. Reader.read()), which parses datum form
 * character stream.
 */

#include "datum.hpp"
#include "lexer.hpp"

#include <cassert>
#include <forward_list>
#include <list>
#include <memory>
#include <optional>

namespace scheme2llvm {
class Reader;
using std::optional;

class Reader {
public:
  struct StackNode {
    LexemeKind kind;
    enum Status {
      NoPeriod,
      LastPeriod,
      MeetPeriod
    } last_period;
    std::list<Datum> sub_datums;
    StackNode(LexemeKind k) : kind(k), last_period(NoPeriod), sub_datums() {}
  };

  class Stack {
    std::forward_list<StackNode> stack;
    size_t stack_size = 0;
  public:
    bool empty() { return !stack_size; }

    LexemeKind frontKind() {
      assert(!stack.empty());
      return stack.front().kind;
    }

    // add pd to be a sub-datum of the stack front
    void frontAdd(Datum &&pd) {
      assert(!stack.empty());
      stack.front().sub_datums.emplace_back(std::move(pd));
    }

    void push(LexemeKind k) {
      ++stack_size;
      stack.emplace_front(k);
    }

    void pop() {
      assert(!stack.empty());
      --stack_size;
      stack.pop_front();
    }

    Datum takeFront() {
      Datum ret;
      assert(!stack.empty());
      switch (frontKind()) {
      case (LK_LeftParentheses):
      case (LK_LeftBracket):
        switch (stack.front().last_period) {
        case (StackNode::NoPeriod):
           ret = DatumList(stack.front().sub_datums.size(), stack.front().sub_datums);
          break;
        case (StackNode::MeetPeriod):
          ret = DatumImproperList(stack.front().sub_datums.size(), stack.front().sub_datums);
          break;
        case (StackNode::LastPeriod):
          ret = DK_Error;
          break;
        default:
          assert(false);
          ret = DK_Error;
        }
        break;
      case (LK_PoundParentheses):
        ret = DatumVector(stack.front().sub_datums.size(), stack.front().sub_datums);
        break;
      case (LK_Pound_vu8_Parentheses):
        ret = ByteVector(stack.front().sub_datums.size(), stack.front().sub_datums);
        break;
      default:
        assert(false);
      }
      pop();
      return ret;
    }

    void clear() { stack.clear(); }
    size_t size() { return stack_size; }

    // predicate: datum is a non-starting/non-ending datum
    optional<Datum> combineStackFront(Datum &&datum);
    // predicate: lexeme is a ending lexeme
    optional<Datum> endStackFront(Lexeme lexeme);
  };

    Reader(Lexer &lexer_) : lexer(&lexer_), stack() {}
    std::optional<Datum> read(LexerPosition &pos);
    void reset(Lexer &lexer_) {
      lexer = &lexer_;
      stack.clear();
  }

private:
  Lexer* lexer;
  Stack stack;
};

} // namespace scheme2llvm

#endif // SCHEME2LLVM_READ
