CXXFLAGS+=$(shell llvm-config --cxxflags|sed 's/-std=c++11/-std=c++17/g; s/-DNDEBUG//g; s/-O2//g; s/-O3//g')
CXXFLAGS+=$(shell pkg-config --cflags icu-uc icu-io icu-i18n)
CXXFLAGS+=-I $(shell pwd)
LDLIBS+=$(shell llvm-config --libs)
LDLIBS+=$(shell pkg-config --libs-only-l icu-uc icu-io icu-i18n)
LDFLAGS+=$(shell llvm-config --ldflags)
LDFLAGS+=$(shell pkg-config --libs-only-L icu-uc icu-io icu-i18n)
build_dir=$(shell pwd)/build

lexer_run.exe: lexer.o test/lexer_run.o
	$(CXX) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

echo.exe: lexer.o datum.o read.o test/echo.o
	$(CXX) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@
