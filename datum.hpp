#ifndef SCHEME2LLVM_DATUM_HPP
#define SCHEME2LLVM_DATUM_HPP

/* This file define data structures for datum syntax
 */

#include "lexer.hpp"
#include "memory.hpp"
#include "number.hpp"

#include "llvm/Support/raw_ostream.h"
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include "boost/core/demangle.hpp"
#include <typeinfo>
#include <type_traits>

namespace scheme2llvm {
struct Datum;
class DatumList;
class Abbreviation;

enum DatumKind {
  // NOTE: bytevector is a compound datum in R6RS, but a simple dataum in R7RS
  // abbreviation is a list in R6RS, but a seperate kind in R7RS
  DK_Boolean,
  DK_Number,
  DK_Character,
  DK_String,
  DK_Symbol,
  DK_ByteVector,
  DK_List,
  DK_ImproperList,
  DK_Vector,
  DK_Abbreviation,
  // The followings are not valid datum kind
  DK_Period,
  DK_Null,  // to signify a datum after move or not get a datum
  DK_Error, // to indicate some error happens when trying to get datum
  DK_Count
};

enum AbbreviationKind {
  // make sure AbbreviationKind don't overlap with DatumKind, so that we
  // can cram them into the same int.
  AbbrK_Apostrophe = DK_Count + 1, // '
  AbbrK_BackQuote,                 // `
  AbbrK_Comma,                     // ,
  AbbrK_CommaAt,                   // ,@
  // NOTE: the followings are R6RS abbreviations but not R7Rs, we accept them
  // as extensions to support syntax-case
  AbbrK_PoundApostrophe, // #'
  AbbrK_PoundBackQuote,  // #`
  AbbrK_PoundComma,      // #,
  AbbrK_PoundCommaAt,    // #,@
  // the followings are not valid AbbreviationKind
  AbbrK_Null, // to signify a Abbreviation after move or not get a datum
  AbbrK_Error
};

inline AbbreviationKind lexemeToAreviationKind(Lexeme lexeme) {
  switch (lexeme.kind) {
  case LK_Apostrophe: // '
    return AbbrK_Apostrophe;
  case LK_BackQuote: // `
    return AbbrK_BackQuote;
  case LK_Comma: // ,
    return AbbrK_Comma;
  case LK_CommaAt: // ,@
    return AbbrK_CommaAt;
  case LK_PoundApostrophe: // #'
    return AbbrK_PoundApostrophe;
  case LK_PoundBackQuote: // #`
    return AbbrK_PoundBackQuote;
  case LK_PoundComma: // #,
    return AbbrK_PoundComma;
  case LK_PoundCommaAt: // #,@
    return AbbrK_PoundCommaAt;
  default:
    assert(false);
    return AbbrK_Error;
  }
}

struct Abbreviation {
  AbbreviationKind kind = AbbrK_Null;
  Datum *sub_datum;
  // d is a owning pointer
  Abbreviation(AbbreviationKind abbr_k, Datum &&d);
  Abbreviation(const Abbreviation &) = delete;
  Abbreviation &operator=(const Abbreviation &) = delete;
  Abbreviation(Abbreviation &&rhs) {
    kind = rhs.kind;
    sub_datum = rhs.sub_datum;
    rhs.kind = AbbrK_Null;
    rhs.sub_datum = nullptr;
  }
  Abbreviation &operator=(Abbreviation &&rhs) {
    std::swap(kind, rhs.kind);
    std::swap(sub_datum, rhs.sub_datum);
    return *this;
  }
  ~Abbreviation();
  void write(llvm::raw_ostream &) const;
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Abbreviation &abbr) {
  abbr.write(os);
  return os;
}

namespace internal {
// predicate: T is a movable type
template <typename T> struct MoveOnlyList {
  T *begin;
  size_t size;
  MoveOnlyList() : begin(nullptr), size(0) {}
  // cont is a container of datum
  template <typename C> MoveOnlyList(size_t s, C &cont) : size(s) {
    if (!s) {
      begin = nullptr;
      return;
    }
    T *p = (begin = allocate<T>(size));
    assert(p);
    for (auto &v : cont) {
      new (p) T(std::move(v));
      ++p;
    }
    cont.clear();
  }
  MoveOnlyList(const MoveOnlyList &) = delete;
  MoveOnlyList &operator=(const MoveOnlyList &) = delete;
  MoveOnlyList(MoveOnlyList &&rhs) {
    begin = rhs.begin;
    size = rhs.size;
    rhs.begin = nullptr;
    rhs.size = 0;
  }
  MoveOnlyList &operator=(MoveOnlyList &&rhs) {
    std::swap(begin, rhs.begin);
    std::swap(size, rhs.size);
    return *this;
  }
  ~MoveOnlyList() {
    for (size_t s = 0; s < size; ++s) {
      begin[s].~T();
    }
    deallocate(begin, size);
  }
  void write(llvm::raw_ostream &os) const {
    for (size_t i = 0; i < size; ++i) {
      begin[i].write(os);
      if (i + 1 != size) {
        os << ' ';
      }
    }
  }
};
template <typename T>
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const MoveOnlyList<T> &l) {
  l.write(os);
  return os;
}
} // namespace internal

struct DatumList : internal::MoveOnlyList<Datum> {
  using internal::MoveOnlyList<Datum>::MoveOnlyList;
  void write(llvm::raw_ostream &) const;
};
struct DatumImproperList : internal::MoveOnlyList<Datum> {
  using internal::MoveOnlyList<Datum>::MoveOnlyList;
  void write(llvm::raw_ostream &) const;
};
struct DatumVector : internal::MoveOnlyList<Datum> {
  using internal::MoveOnlyList<Datum>::MoveOnlyList;
  void write(llvm::raw_ostream &) const;
};
struct ByteVector : internal::MoveOnlyList<uint8_t> {
  // cont is a container of datum
  template <typename C> ByteVector(size_t s, C &cont) {
    size = s;
    uint8_t *p = (begin = allocate<uint8_t>(size));
    for (auto &v : cont) {
      *p++ = v.number.real_part.integer.getSExtValue();
    }
    cont.clear();
  }
  void write(llvm::raw_ostream &) const;
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const DatumList &l) {
  l.write(os);
  return os;
}

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os,
                              const DatumImproperList &il) {
  il.write(os);
  return os;
}

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const DatumVector &v) {
  v.write(os);
  return os;
}

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const ByteVector &bv) {
  bv.write(os);
  return os;
}

// this is effectively a variant, but I decided to implement it myself
struct Datum {
  DatumKind kind;
  union {
    bool boolean;
    char32_t character;
    ComplexNumber number;
    StringRepresentation string;
    IdentifierRepresentation symbol;
    DatumList list;
    DatumImproperList impr_list;
    DatumVector vector;
    ByteVector byte_vector;
    Abbreviation abbr;
  };

  void destroy() {
    switch (kind) {
    case DK_Boolean:
    case DK_Character:
    case DK_Null:
    case DK_Error:
    case DK_Period:
    case DK_Count:
      break;
    case DK_Number:
      number.~ComplexNumber();
      break;
    case DK_String:
      string.~StringRepresentation();
      break;
    case DK_Symbol:
      symbol.~IdentifierRepresentation();
      break;
    case DK_List:
      list.~DatumList();
      break;
    case DK_ImproperList:
      impr_list.~DatumImproperList();
      break;
    case DK_Vector:
      vector.~DatumVector();
      break;
    case DK_ByteVector:
      byte_vector.~ByteVector();
      break;
    case DK_Abbreviation:
      abbr.~Abbreviation();
      break;
    default:
      assert(false);
    }
  }

public:
  Datum(DatumKind dk = DK_Null) : kind(dk) {}

  template <typename Tcv> Datum(Tcv &&v, DatumKind k = DK_Null) {
    using T = std::remove_reference_t<std::remove_cv_t<Tcv>>;
    if constexpr (std::is_same_v<T, bool>) {
      assert(k == DK_Boolean || k == DK_Null);
      kind = DK_Boolean;
      boolean = std::move(v);
    } else if constexpr (std::is_same_v<T, char32_t>) {
      assert(k == DK_Character || k == DK_Null);
      kind = DK_Character;
      character = std::move(v);
    } else if constexpr (std::is_same_v<T, ComplexNumber>) {
      assert(k == DK_Number || k == DK_Null);
      kind = DK_Number;
      new (&number) ComplexNumber(std::move(v));
    } else if constexpr (std::is_same_v<T, StringRepresentation>) {
      assert(k == DK_String || k == DK_Symbol);
      kind = k;
      if (k == DK_String) {
        new (&string) StringRepresentation(std::move(v));
      } else {
        new (&symbol) StringRepresentation(std::move(v));
      }
    } else if constexpr (std::is_same_v<T, DatumList>) {
      assert(k == DK_List || k == DK_Null);
      kind = DK_List;
      new (&list) DatumList(std::move(v));
    } else if constexpr (std::is_same_v<T, DatumImproperList>) {
      assert(k == DK_ImproperList || k == DK_Null);
      kind = DK_ImproperList;
      new (&impr_list) DatumImproperList(std::move(v));
    } else if constexpr (std::is_same_v<T, DatumVector>) {
      assert(k == DK_Vector || k == DK_Null);
      kind = DK_Vector;
      new (&vector) DatumVector(std::move(v));
    } else if constexpr (std::is_same_v<T, ByteVector>) {
      assert(k == DK_ByteVector || k == DK_Null);
      kind = DK_ByteVector;
      new(&byte_vector) ByteVector(std::move(v));
    } else if constexpr (std::is_same_v<T, Abbreviation>) {
      assert(k == DK_Abbreviation || k == DK_Null);
      kind = DK_Abbreviation;
      new (&abbr) Abbreviation(std::move(v));
    } else {
      assert(false);
    }
  }

  // construct a simple datum from a lexeme
  Datum(Lexeme lexeme) {
    switch (lexeme.kind) {
    case LK_Identidier:
      kind = DK_Symbol;
      new (&symbol) IdentifierRepresentation(lexeme.identifier);
      return;
    case LK_String:
      kind = DK_String;
      new (&string) IdentifierRepresentation(lexeme.string);
      return;
    case LK_Number:
      kind = DK_Number;
      new (&number) ComplexNumber(lexeme.number);
      return;
    case LK_Boolean:
      kind = DK_Boolean;
      boolean = lexeme.boolean;
      return;
    case LK_Character:
      kind = DK_Character;
      character = lexeme.character;
      return;
    case LK_Period:
      kind = DK_Period;
      return;
    default:
      assert(false);
    }
  }

  // lifetime management boilerplates
  Datum(const Datum &) = delete;
  Datum &operator=(const Datum &) = delete;

  Datum(Datum &&other) {
    kind = other.kind;
    switch (kind) {
    case DK_Boolean:
      boolean = other.boolean;
      break;
    case DK_Character:
      character = other.character;
      break;
    case DK_Number:
      new (&number) ComplexNumber(std::move(other.number));
      break;
    case DK_String:
      new (&string) StringRepresentation(std::move(other.string));
      break;
    case DK_Symbol:
      new (&symbol) IdentifierRepresentation(std::move(other.symbol));
      break;
    case DK_List:
      new (&list) DatumList(std::move(other.list));
      break;
    case DK_ImproperList:
      new (&impr_list) DatumImproperList(std::move(other.impr_list));
      break;
    case DK_Vector:
      new (&vector) DatumVector(std::move(other.vector));
      break;
    case DK_ByteVector:
      new (&byte_vector) ByteVector(std::move(other.byte_vector));
      break;
    case DK_Abbreviation:
      new (&abbr) Abbreviation(std::move(other.abbr));
      break;
    case DK_Null:
    case DK_Error:
    case DK_Period:
    case DK_Count:
      break;
    default:
      assert(false);
    }
    other.kind = DK_Null;
  }

  Datum &operator=(Datum &&rhs) {
    destroy();
    new (this) Datum(std::move(rhs));
    return *this;
  }

  ~Datum() { destroy(); }

  template <typename T> T &get(DatumKind k = DK_Null) {
    if constexpr (std::is_same_v<T, bool>) {
      assert(k == DK_Boolean || k == DK_Null);
      return boolean;
    } else if constexpr (std::is_same_v<T, char32_t>) {
      assert(k == DK_Character || k == DK_Null);
      return character;
    } else if constexpr (std::is_same_v<T, ComplexNumber>) {
      assert(k == DK_Number || k == DK_Null);
      return number;
    } else if constexpr (std::is_same_v<T, StringRepresentation>) {
      assert(k == DK_String || k == DK_Symbol);
      if (k == DK_String) {
        return string;
      } else {
        return symbol;
      }
    } else if constexpr (std::is_same_v<T, DatumList>) {
      assert(k == DK_List || k == DK_Null);
      return list;
    } else if constexpr (std::is_same_v<T, DatumImproperList>) {
      assert(k == DK_ImproperList || k == DK_Null);
      return impr_list;
    } else if constexpr (std::is_same_v<T, DatumVector>) {
      assert(k == DK_Vector || k == DK_Null);
      return vector;
    } else if constexpr (std::is_same_v<T, ByteVector>) {
      assert(k == DK_ByteVector || k == DK_Null);
      return byte_vector;
    } else if constexpr (std::is_same_v<T, Abbreviation>) {
      assert(k == DK_Abbreviation || k == DK_Null);
      return abbr;
    } else {
      assert(false);
      return T();
    }
  }

  template <typename T> void set(T &&v, DatumKind k = DK_Null) {
    if constexpr (std::is_same_v<T, bool>) {
      assert(k == DK_Boolean || k == DK_Null);
      boolean = std::move(v);
    } else if constexpr (std::is_same_v<T, char32_t>) {
      assert(k == DK_Character || k == DK_Null);
      character = std::move(v);
    } else if constexpr (std::is_same_v<T, ComplexNumber>) {
      assert(k == DK_Number || k == DK_Null);
      number = std::move(v);
    } else if constexpr (std::is_same_v<T, StringRepresentation>) {
      assert(k == DK_String || k == DK_Symbol);
      if (k == DK_String) {
        string = std::move(v);
      } else {
        symbol = std::move(v);
      }
    } else if constexpr (std::is_same_v<T, DatumList>) {
      assert(k == DK_List || k == DK_Null);
      list = std::move(v);
    } else if constexpr (std::is_same_v<T, DatumImproperList>) {
      assert(k == DK_ImproperList || k == DK_Null);
      impr_list = std::move(v);
    } else if constexpr (std::is_same_v<T, DatumVector>) {
      assert(k == DK_Vector || k == DK_Null);
      vector = std::move(v);
    } else if constexpr (std::is_same_v<T, ByteVector>) {
      assert(k == DK_ByteVector || k == DK_Null);
      byte_vector = std::move<T>(v);
    } else if constexpr (std::is_same_v<T, Abbreviation>) {
      assert(k == DK_Abbreviation || k == DK_Null);
      abbr = std::move(v);
    } else {
      assert(false);
    }
  }

  DatumKind getKind() { return kind; }
  void write(llvm::raw_ostream &) const;
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &os, const Datum &d) {
  d.write(os);
  return os;
}

} // namespace scheme2llvm

#endif // SCHEME2LLVM_DATUM_HPP
