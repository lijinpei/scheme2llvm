#include "lexer.hpp"

#include "llvm/Support/raw_ostream.h"
#include <iostream>
#include <optional>
#include <cstddef>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

using namespace scheme2llvm;
int main() {
  size_t size = lseek(0, 0, SEEK_END);
  lseek(0, 0, SEEK_SET);
  std::cerr << "input stream size: " << size << std::endl;
  char* buf;
  posix_memalign(&(void*&)buf, getpagesize(), size);
  read(0, buf, size);
  char* buf_end = buf + size;
  LexerPosition pos(buf, buf_end);
  Lexer lexer;
  std::cerr << "buf: " << (void*)buf << std::endl;
  std::cerr << "buf_end: " << (void*) buf_end << std::endl;
  std::cerr << "start echo\n";
  for (char* p = buf; p < buf_end; ++p) {
    std::cerr << *p;
  }
  std::cerr << "end echo\n";
  //int i = 0;
  while (pos) {
    //++i;
    //std::cerr << "count: " << i;
    auto ret = lexer.lex(pos);
    if (ret) {
      ret->print(llvm::outs());
    } else {
      break;
    }
    llvm::outs() << ' ';
  }
}
