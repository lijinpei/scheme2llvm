#ifndef SCHEME2LLVM_MEMORY_HPP
#define SCHEME2LLVM_MEMORY_HPP

#include "stdlib.h"

namespace scheme2llvm {
// num is number of T, not number of bytes
template <typename T> T *allocate(size_t num) {
  if (!num) {
    return nullptr;
  }
  if (void *ret = malloc(sizeof(T) * num)) {
    assert(ret);
    return reinterpret_cast<T *>(ret);
  } else {
    return nullptr;
  }
}

template <typename T> bool deallocate(T *p, size_t) {
  if (p) {
    free(p);
  }
  return true;
}
} // namespace scheme2llvm

#endif // SCHEME2LLVM_MEMORY_HPP
