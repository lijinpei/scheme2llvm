#ifndef SCHEME2LLVM_UTF8_HPP
#define SCHEME2LLVM_UTF8_HPP

#include "config.hpp"

//#include "unicode/utf8.h"
#include <cstddef>
#include <iterator>
#include <iostream>
#include <ios>

namespace scheme2llvm {
namespace internal {
// shamelessly stolen from unicode/utf8.h
inline char32_t u8_next(char *&p) {
  char32_t c;
  c = uint8_t(*p++);
#define U8_IS_SINGLE(c) (((c)&0x80)==0)
  if (!U8_IS_SINGLE(c)) {
    if (c < 0xe0) {
      c = ((c & 0x1f) << 6) | (*p++ & 0x3f);
    } else if (c < 0xf0) {
      c = ((c&0xf) << 12) | ((*p & 0x3f) << 6) | (*(p + 1) & 0x3f);
      p += 2;
    } else {
      c = ((c & 7) << 18) | ((*p & 0x3f) << 12) | ((*(p + 1) & 0x3f) << 6) |
          (*(p + 2) & 0x3f);
      p += 3;
    }
  }
  return c;
#undef U8_IS_SINGLE
}

inline char32_t u8(char *p) { return u8_next(p); }
} // namespace internal

class utf8_ptr {
  char *p, *pe;
public:
  utf8_ptr(char *p_, char *pe_) : p(p_), pe(pe_) {}
  char32_t operator++(int) {
    if (p < pe) {
      return internal::u8_next(p);
    } else {
      return U'\0';
    }
  }
  operator bool() {
    return p < pe;
  }
  char* pos() {return p;}
  char* pos_end() {return pe;}
  bool set_pos(char* np) {
    p = np;
    return true;
  }
};
} // namespace scheme2llvm

#endif // SCHEME2LLVM_UTF8_HPP
